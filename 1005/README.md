# 1005. Куча камней
У вас есть несколько камней известного веса w1, …, wn. Напишите программу, которая распределит камни в две кучи так, что разность весов этих двух куч будет минимальной.

## Исходные данные
Ввод содержит количество камней n (1 ≤ n ≤ 20) и веса камней w1, …, wn (1 ≤ wi ≤ 100 000) — целые, разделённые пробельными символами.

## Результат
Ваша программа должна вывести одно число — минимальную разность весов двух куч.

## Пример
#### Исходные данные
```bash
5
5 8 13 27 14
```
#### Результат
```bash
3
```



