package main

import (
	"fmt"
	"testing"
	"os"
	"encoding/json"
)

type TestCases struct {
	Cases []struct {
		Input  []uint32 `json:"input"`
		Ans uint32   `json:"answer"`
	} `json:"cases"`
}


func loadCases(tests *TestCases) error {
	f, err := os.Open("fixtures.json")
	if err != nil {
		return err
	}
	defer f.Close()

	dec := json.NewDecoder(f)
	err = dec.Decode(tests)

	if err != nil {
		return err
	}


	return nil
}

func TestSolveFunc(t *testing.T) {

	cases := new(TestCases)
	err := loadCases(cases)

	if err != nil {
		fmt.Println(err)
	}

	for _, test := range cases.Cases {
		realAns := Solve(test.Input)

		if test.Ans != realAns {
			t.Errorf("Returned: %d for %v\nExpected: %d", realAns, test.Input, test.Ans)
		}
	}
	return
}

var bCase = []uint32{806, 2500, 68, 1231, 100, 8, 781, 311, 95, 8, 1343, 4, 55, 834, 113, 432, 220, 5, 17, 21}
var result uint32

func benchmarkSolve(b *testing.B, w []uint32) {
	var r uint32
	for i := 0; i < b.N; i++ {
		r = Solve(w)
	}
	result = r
}

func Benchmark1solve(b *testing.B) {
	benchmarkSolve(b, bCase)
}

