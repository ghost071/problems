package main

import (
	"fmt"
	"sort"
)

func readStd() (error, int, []uint32) {
	var size int
	_, err := fmt.Scanf("%d\n", &size)

	weights := make([]uint32, size)

	if err != nil {
		return err, size, weights
	}

	for i, _ := range weights {
		_, err := fmt.Scanf("%d", &weights[i])
		if err != nil {
			return err, size, weights
		}
	}
	return nil, size, weights
}

type sortSet struct {
	data []uint32
}

func (s *sortSet) Len() int {
	return len(s.data)
}

func (s *sortSet) Less(i, j int) bool {
	if s.data[i] < s.data[j] {
		return true
	}
	return false
}

func (s *sortSet) Swap(i, j int) {
	s.data[i], s.data[j] = s.data[j], s.data[i]
}

func FindSubsets(w *[]uint32, first, last int) *[]uint32 {
	size := 1 << (last - first)
	subsets := make([]uint32, size)

	for i := 0; i < size; i++ {
		var sum uint32
		for k := 0; k <= last - first; k++ {
			if i & (1 << k) != 0 {
				sum += (*w)[k+first]
			}
		}
		subsets[i] = sum
	}
	sort.Sort(&sortSet{subsets})
	return &subsets
}

func Solve(weights []uint32) uint32 {
	if len(weights) == 1 {
		return weights[0]
	}

	sort.Sort(&sortSet{weights})
	var totalSum uint32
	for _, w := range weights {
		totalSum += w
	}

	halfSum := totalSum / 2
	size := len(weights)
	weights1 := FindSubsets(&weights, 0, size/2)
	weights2 := FindSubsets(&weights, size/2, size)

	var upperBound, lowerBound uint32
	upperBound = totalSum

	for i := len(*weights1) - 1; i >= 0; i-- {
		for j := 0; j < len(*weights2); j++ {
			sum := (*weights1)[i] + (*weights2)[j]

			if sum >= halfSum && sum < upperBound {
				upperBound = sum
				if 2 * sum == totalSum {
					return 0
				}
			}

			if sum <= halfSum && sum > lowerBound {
				lowerBound = sum
				if sum == halfSum {
					return totalSum - 2 * lowerBound
				}
			}
		}
	}
	return totalSum - 2 * lowerBound
}

func main() {
	err, _, weights := readStd()
	if err != nil {
		panic(err)
	}

	answer := Solve(weights)
	fmt.Println(answer)
}

